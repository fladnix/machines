{ pkgs, config, ... }: {

  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;
  programs.bash.enable = true; # let home-manage profile
  programs.keychain.enable = true;
  programs.keychain.enableBashIntegration = true;

  home.packages = with pkgs; [
    coreutils
    ripgrep
    tree
    git
    skim
    xe
    fzf
    file
    xplr
    zellij
    wget
    chafa
    fd
    nixfmt
    nixpkgs-fmt
    wireguard-tools
    bluez
    bluez-tools
    # yewtube # apparently only on unstable
    youtube-dl
    yt-dlp
    rename
    python3
    sshfs
    encfs
    unzip
    udiskie
    libsixel
    SDL_sixel
    amfora
    xdg-utils
    kubectl
    unison
    mpv
    aerc
    w3m
    lynx # to convert html to text for aerc
    rbw
    weechat
    # unison-lang#ucm
  ];

  xdg.configFile.all = {
    source = ../../config;
    target = ".";
    recursive = true;
  };

  home.file.share = {
    source = ../../share;
    target = ".local/share";
    recursive = true;
  };

  systemd.user.services.udiskie = {
    Unit.Description = "Udiskie Automount usb";
    Service.ExecStart = "${pkgs.udiskie}/bin/udiskie";
    Install.WantedBy = [ "default.target" ];
  };

  imports = [
    ./gpg.nix
    ./yubikey.nix
    ./kakoune.nix
    ./sync.nix
    ./multi-git-status.nix
    ./java.nix
    ./dotnet.nix
    ./haskell.nix
  ];

  services.syncthing.enable = true;
}
