{ pkgs, ... }:
{
  home.packages = with pkgs; [
      kakoune
      kak-lsp
      rnix-lsp
  ];
  xdg.configFile.kak = {
      source = ./config/kak;
      target = "kak";
      recursive = true;
  };
}
