{ pkgs, pkgs-unstable, pkgs-postman, ... }: {

  home.packages = with pkgs; [
	  openconnect
      networkmanager-openconnect
      networkmanager-openvpn
      gnome.networkmanager-openconnect
      gnome.networkmanager-openvpn
      globalprotect-openconnect
      pkgs-unstable.jetbrains.idea-ultimate
      pkgs-unstable.jetbrains.rust-rover
      jdk17
      awscli
      docker-compose
      docker
      parsec-bin
      localstack
      openlens
      kubelogin-oidc
      kubectl
      kubectx
      k9s
      pkgs-postman.postman
  ];

  home.sessionVariables."JAVA_HOME" = "${pkgs.jdk17.home}";
}
