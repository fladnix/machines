{ pkgs, ... }: {
  home.packages = [ pkgs.foot ];

  #   xdg.configFile."foot/foot.ini".source = ../../config/foot/foot.ini;

  home.shellAliases.ssh =
    "TERM=linux ssh"; # prevent broken SSH terminal with foo

  home.sessionVariables.TERMINAL = "foot";

}
