{ ... }: {
  xdg.mime.enable = true;
  targets.genericLinux.enable = true;
}
