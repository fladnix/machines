{ lib, stdenv, pkgs, ... }:
let
  wallpaper = pkgs.writeShellApplication {
    name = "wallpaper";
    runtimeInputs = [ pkgs.coreutils pkgs.sway ];
    text = builtins.readFile ../../scripts/wallpaper;
  };
in rec { home.packages = [ wallpaper ]; }

