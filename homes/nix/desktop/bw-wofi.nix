{ lib, stdenv, pkgs, ... }:
let
  bw-wofi = pkgs.writeShellApplication {
    name = "bw-wofi";
    runtimeInputs = [
        pkgs.coreutils
        pkgs.rbw
        pkgs.wofi
        pkgs.bash
        pkgs.pinentry-gtk2
        pkgs.wl-clipboard
    ];
    text = builtins.readFile ../../scripts/bw-wofi;
  };
in rec { home.packages = [ bw-wofi ]; }

