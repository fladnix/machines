{ lib, stdenv, pkgs, ... }:
let
  waybar-git = pkgs.writeShellApplication {
    name = "waybar-git";
    runtimeInputs = [ pkgs.coreutils ];
    text = builtins.readFile ../../scripts/waybar-git;
  };
in rec { home.packages = [ waybar-git ]; }
