{ pkgs, pkgs-unstable, ... }: {

  home.packages = with pkgs; [
    pavucontrol
    inkscape
    alacritty
    numix-cursor-theme
    gnome.dconf-editor
    gsettings-desktop-schemas
    nushell
    wireplumber
    pamixer
    pkgs-unstable.bitwarden
    tor-browser-bundle-bin
    transmission-gtk
    zathura
    android-file-transfer
    imagemagick
    home-assistant
    openscad
    gimp
    xsane
    sane-frontends
    sane-backends
    libreoffice
    # minikube
    # unison-ucm
    # kind
    pkgs-unstable.mullvad-vpn
    slack
    android-tools
    google-chrome
    pkgs-unstable.mullvad-browser
    lagrange
    thunderbird
    qutebrowser
    pkgs-unstable.firefox
    signal-desktop
    jetbrains.rider
	networkmanagerapplet
	system-config-printer
	zoom-us
  ];

  imports = [
	  ./fonts.nix
	  ./kubernetes.nix
	  ./games.nix
  ];
}
