{ lib, stdenv, pkgs, ... }:
let
  waybar-disk = pkgs.writeShellApplication {
    name = "waybar-disk";
    runtimeInputs = [ pkgs.coreutils ];
    text = builtins.readFile ../../scripts/waybar-disk;
  };
in rec { home.packages = [ waybar-disk ]; }

