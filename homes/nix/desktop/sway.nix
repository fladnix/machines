{ pkgs, pks-unstable, catppuccin, ... }: {
  home.packages = with pkgs; [
    sway
    wl-clipboard
    swayidle
    swaylock
    mako
    dmenu
    wofi
    libnotify
    xdg-desktop-portal
    xdg-desktop-portal-hyprland
    xdg-desktop-portal-wlr
    xdg-desktop-portal-gtk
    xdg-desktop-portal-gnome
    pipewire
    wireplumber
    grim
    wf-recorder
    slurp
    light
    waybar
    wdisplays
  ];

  # wayland.windowManager.sway.enable = true;
  # wayland.windowManager.sway.extraConfig = builtins.readFile ../../config/sway/config;

  home.sessionVariables = {
    MOZ_ENABLE_WAYLAND = "1";
    XDG_CURRENT_DESKTOP = "sway";
    AWT_TOOLKIT = "Mtoolkit";
    _JAVA_AWT_WM_NONREPARENTING =
      "1"; # avoid blank screen on java ide like jetbrains
    WLR_NO_HARDWARE_CURSORS = "1"; # fix bug non displaying mouse cursor
  };

  #   xdg.configFile."sway/config".source = ../../config/sway/config;
  xdg.dataHome."icons/default/index.theme".source = ../../config/index.theme;
  gtk = {
	  enable = true;
	  catppuccin =  {
		  enable = true;
		  flavor = "macchiato";
		  accent = "blue";
		  gnomeShellTheme = true;
		  icon = {
			  enable = true;
			  accent = "blue";
			  flavor = "macchiato";
		  };
		  
	  };
  };
  # gtk.cursorTheme.package = pkgs.numix-cursor-theme;
  # gtk.cursorTheme.name = "Numix-Cursor";

}
