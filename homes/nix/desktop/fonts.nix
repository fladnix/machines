{ pkgs, ... }: {
  home.packages = with pkgs; [
    powerline-fonts
    material-design-icons
    source-code-pro
    fira-code
    noto-fonts-emoji
    jetbrains-mono
  ];

  fonts.fontconfig.enable = true;
}
