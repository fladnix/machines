{ lib, stdenv, pkgs, ... }:
let
  wallpaper = pkgs.writeShellApplication {
    name = "sxiv-handler";
    runtimeInputs = [ pkgs.coreutils ];
    text = builtins.readFile ../../scripts/sxiv-key-handler;
  };
in rec { home.packages = [ pkgs.sxiv ]; }
