{ ... }: {
  imports = [
    ./foot.nix
    ./fonts.nix
    ./sway.nix
    ./waybar-disk.nix
    ./waybar-git.nix
    ./wallpaper.nix
    ./sxiv.nix
    ./bw-wofi.nix
  ];
}
