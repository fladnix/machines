{ pkgs, ...}:{
	home.packages = [
		pkgs.dotnet-sdk_7
		pkgs.omnisharp-roslyn
	];
	home.sessionVariables."DOTNET_ROOT" = "{pkgs.dotnet-sdk_7}";
}
