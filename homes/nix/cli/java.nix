{ pkgs, config, ...}:
{
    home.packages = [
        pkgs.openjdk17
        pkgs.jdt-language-server
        pkgs.gradle
    ];
}
