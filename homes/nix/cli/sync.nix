{ lib, stdenv, pkgs, ... }:
let
  sync-exclude-list = pkgs.writeShellApplication {
    name = "sync-exclude-list";
    runtimeInputs = [ pkgs.coreutils pkgs.fd pkgs.bash pkgs.openssh ];
    text = builtins.readFile ../../scripts/sync-exclude-list;
  };
  sync-dry-run = pkgs.writeShellApplication {
    name = "sync-dry-run";
    runtimeInputs = [ sync-exclude-list pkgs.coreutils pkgs.unison ];
    text = builtins.readFile ../../scripts/sync-dry-run;
  };
in rec { home.packages = [ sync-exclude-list sync-dry-run ]; }

