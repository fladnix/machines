{ pkgs, ... }:
{
  home.packages = with pkgs; [
      kakoune
      kak-lsp
  ];
  xdg.configFile.kak = {
      source = ../../config/kak;
      target = "kak";
      recursive = true;
  };
}
