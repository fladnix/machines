{ pkgs, ... }:
{
  home.packages = with pkgs; [
    yubikey-manager
    pinentry-gtk2
    yubikey-agent
    yubikey-touch-detector
  ];
}
