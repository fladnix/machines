{ pkgs, ...}:{
	home.packages = [
		pkgs.starship
	];
	programs.bash.bashrcExtra = ''
	eval "$(starship init bash)"
	'';
}
