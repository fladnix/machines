{ pkgs, ... }:
let
  mgs = pkgs.stdenv.mkDerivation {
    name = "multi-git-status";
    src = pkgs.fetchFromGitHub {
      owner = "fboender";
      repo = "multi-git-status";
      rev = "852ee02281a20eeb3aa5780a32a4136663c8e08d";
      sha256 = "1n12mn3h2gaqdqcr14vbq3fm8n06das887blfmafvxza8zn1fflg";
    };
    installPhase = ''
      PREFIX=$out make install
    '';
  };
in { home.packages = [ mgs ]; }
