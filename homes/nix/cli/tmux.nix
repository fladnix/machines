{ pkgs, pkgs-unstable, config, ... }: {

  programs.tmux = {
	  enable = true;
	  plugins = with pkgs; [
	  {
		  plugin = tmuxPlugins.catppuccin;
	  }
	  {
		  plugin = tmuxPlugins.sensible;
	  }
	  {
		  plugin = tmuxPlugins.battery;
	  }
	  ];
	  extraConfig = ''
		set -g base-index 1
		setw -g pane-base-index 1
		set-option -g status-position top

		set -g @catppuccin_flavour 'macchiato'
		set -g @catppuccin_window_left_separator ""
		set -g @catppuccin_window_right_separator " "
		set -g @catppuccin_window_middle_separator " █"
		set -g @catppuccin_window_number_position "right"
		set -g @catppuccin_window_default_fill "number"
		set -g @catppuccin_window_default_text "#W"
		set -g @catppuccin_window_current_fill "number"
		set -g @catppuccin_window_current_text "#W"
		set -g @catppuccin_status_modules_right "directory user host session battery"
		set -g @catppuccin_status_left_separator  " "
		set -g @catppuccin_status_right_separator ""
		set -g @catppuccin_status_fill "icon"
		set -g @catppuccin_status_connect_separator "no"
		set -g @catppuccin_directory_text "#{pane_current_path}"
		'';
  };
}
