{
  description = "A full desktop environment";

  inputs = {
    # Specify the source of Home Manager and Nixpkgs.
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    unison-lang.url = "github:ceedubs/unison-nix#ucm";
  };

  outputs = { nixpkgs, home-manager, unison-lang, ... }:
    let
      system = "x86_64-linux";
      allowUnfree = { nixpgks.config.allowUnfree = true; };
    in {
      homeConfigurations = {
        animus-desktop-epic = home-manager.lib.homeManagerConfiguration {
          pkgs = import nixpkgs {
            inherit system;
            config.allowUnfree = true;
          };

          extraSpecialArgs = { inherit nixpkgs system unison-lang; };

          modules = [
            ./nix/cli/cli.nix
            ./nix/desktop/desktop.nix
            ./nix/desktop/generic-linux.nix
            ./nix/work/work.nix
          ];
        };

        animus-desktop-generic = home-manager.lib.homeManagerConfiguration {
          pkgs = import nixpkgs {
            inherit system;
            config.allowUnfree = true;
          };

          extraSpecialArgs = { inherit nixpkgs system; };

          modules = [
            ./nix/cli/cli.nix
            ./nix/desktop/desktop.nix
            ./nix/desktop/generic-linux.nix
          ];
        };

        animus-desktop-nixos = home-manager.lib.homeManagerConfiguration {
          pkgs = import nixpkgs {
            inherit system;
            config.allowUnfree = true;
          };

          modules = [ ./nix/cli/cli.nix ./nix/desktop/desktop.nix ];
        };
      };
    };
}
