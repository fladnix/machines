footssh()
{
	if [ "$TERM" -eq "foot" ]; then
		TERM=linux ssh $@
	else
		ssh $@
	fi
}

alias ssh=footssh

eval `keychain --eval --agents ssh id_rsa`

(echo; echo 'eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"') >> /home/animus/.profile
