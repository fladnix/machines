{
  inputs = {
    nixpkgs-postman.url = "github:Nixos/nixpkgs/0aca8f43c8dba4a77aa0c16fb0130237c3da514c";
    nixpkgs.url = "github:Nixos/nixpkgs/nixos-24.05";
    unstable.url = "github:Nixos/nixpkgs/nixos-unstable";
    home-manager = {
      url = "github:nix-community/home-manager/release-24.05";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    gitServer.url = "gitlab:fladnix/git-server";
    remoteLuksUnlock.url = "gitlab:fladnix/remote-luks-unlock";
    unison-lang.url = "github:ceedubs/unison-nix";
    catppuccin.url = "github:catppuccin/nix";
  };

  outputs = { self, nixpkgs, nixpkgs-postman, catppuccin, home-manager, gitServer, remoteLuksUnlock, unison-lang, unstable, ... }@attrs:
    let
    system = "x86_64-linux";
    pkgs = import nixpkgs {
            inherit system;
            config.allowUnfree = true;
          };
    pkgs-unstable = import unstable {
	    	inherit system;
	    	config.allowUnfree = true;
	    };
    pkgs-postman = import nixpkgs-postman {
	    	inherit system;
	    	config.allowUnfree = true;
	    };
    specialArgs = { inherit unison-lang nixpkgs system unstable pkgs-unstable pkgs-postman catppuccin; };
    in {

      # Nixos Machines
      nixosConfigurations = {
        slim = nixpkgs.lib.nixosSystem {
          inherit system specialArgs;
          modules = [
            ./machines/slim/configuration.nix
            home-manager.nixosModules.home-manager
            {
              home-manager.users.animus.imports = [
                ./homes/nix/home-animus.nix
                ./homes/nix/cli/cli.nix
                ./homes/nix/desktop/desktop.nix
                ./homes/nix/desktop/desktop-sway.nix
	            ./homes/nix/work/work.nix
	            catppuccin.homeManagerModules.catppuccin
              ];

              home-manager.users.fuen.imports = [
                ./homes/nix/home-fuen.nix
                ./homes/nix/cli/cli.nix
                ./homes/nix/desktop/desktop.nix
              ];
              home-manager.useGlobalPkgs = true;
              #home-manager.useUserPackages = true;
              home-manager.extraSpecialArgs = specialArgs;
            }
          ];
        };

        darky = nixpkgs.lib.nixosSystem {
          inherit system;
          modules = [
            ./machines/darky/configuration.nix
            home-manager.nixosModules.home-manager
            {
              home-manager.users.animus.imports = [
                ./homes/nix/home-animus.nix
                ./homes/nix/cli/cli.nix
                ./homes/nix/desktop/desktop.nix
                ./homes/nix/desktop/desktop-sway.nix
	            catppuccin.homeManagerModules.catppuccin
              ];

              home-manager.users.fuen.imports = [
                ./homes/nix/home-fuen.nix
                ./homes/nix/cli/cli.nix
                ./homes/nix/desktop/desktop.nix
              ];
              home-manager.useGlobalPkgs = true;
              #home-manager.useUserPackages = true;
            }
          ];
        };

        metal = nixpkgs.lib.nixosSystem {
          inherit system specialArgs;
          modules = [
            ./machines/metal/configuration.nix
            home-manager.nixosModules.home-manager
            {
              home-manager.users.animus.imports = [
                ./homes/nix/home-animus.nix
                ./homes/nix/cli/cli.nix
                ./homes/nix/desktop/desktop.nix
                ./homes/nix/desktop/desktop-sway.nix
	            catppuccin.homeManagerModules.catppuccin
              ];

              home-manager.users.fuen.imports = [
                ./homes/nix/home-fuen.nix
                ./homes/nix/cli/cli.nix
                ./homes/nix/desktop/desktop.nix
              ];
              home-manager.useGlobalPkgs = true;
              #home-manager.useUserPackages = true;
              home-manager.extraSpecialArgs = specialArgs;
            }
          ];
        };

        tiny = nixpkgs.lib.nixosSystem {
          inherit system;
          unstable = pkgs-unstable;
          modules = [
            ./machines/tiny/configuration.nix
            gitServer.nixosModules.gitServer
            remoteLuksUnlock.nixosModules.remoteLuksUnlock
            home-manager.nixosModules.home-manager
            {
              home-manager.users.animus.imports = [
                ./homes/nix/home-animus.nix
                ./homes/nix/cli/cli.nix
                ./homes/nix/desktop/desktop.nix
                ./homes/nix/desktop/desktop-sway.nix
	            catppuccin.homeManagerModules.catppuccin
              ];

              home-manager.users.fuen.imports = [
                ./homes/nix/home-fuen.nix
                ./homes/nix/cli/cli.nix
                ./homes/nix/desktop/desktop.nix
              ];
              home-manager.useGlobalPkgs = true;
              #home-manager.useUserPackages = true;
            }
          ];
        };

      };

      # Home-Manager configurations
      homeConfigurations = {
        animus-desktop-epic = home-manager.lib.homeManagerConfiguration {
         pkgs = pkgs;

          extraSpecialArgs = specialArgs;

          modules = [
            ./homes/nix/home-animus.nix
            ./homes/nix/cli/cli.nix
            ./homes/nix/desktop/desktop.nix
            ./homes/nix/desktop/generic-linux.nix
            ./homes/nix/work/work.nix
            ./homes/nix/desktop/desktop-sway.nix
            catppuccin.homeManagerModules.catppuccin
          ];
        };

        animus-desktop-generic = home-manager.lib.homeManagerConfiguration {
         pkgs = pkgs;

          extraSpecialArgs = specialArgs;

          modules = [
            ./homes/nix/home-animus.nix
            ./homes/nix/cli/cli.nix
            ./homes/nix/desktop/desktop.nix
            ./homes/nix/desktop/generic-linux.nix
            ./homes/nix/desktop/desktop-sway.nix
            catppuccin.homeManagerModules.catppuccin
          ];
        };

        animus-desktop-nixos = home-manager.lib.homeManagerConfiguration {
         pkgs = pkgs;

          extraSpecialArgs = specialArgs;

          modules = [
            ./homes/nix/home-animus.nix
            ./nix/cli/cli.nix
            ./nix/desktop/desktop.nix
            ./homes/nix/desktop/desktop-sway.nix
            catppuccin.homeManagerModules.catppuccin
          ];
        };
      };
    };
}
