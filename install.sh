#!/usr/bin/env bash
set -euo pipefail

ROOT_DISK="nvme0n1"
SWAP_SIZE="8G"
REPO_FLAKE="https://gitlab.com/fladnix/machines.git"
FLAKE_URL="gitlab:fladnix/machines"
MACHINE="slim"

confirm() {
    # call with a prompt string or use a default
    read -r -p "${1:-Are you sure? [y/N]} " response
    case "$response" in
        [yY][eE][sS]|[yY]) 
            true
            ;;
        [yY])
            true
            ;;
        *)
            false
            ;;
    esac
}

# Asking entries

echo "Here are disk options:"
lsblk -d
read -p "Which disk the installation is for?" ROOT_DISK
confirm "This will install on $ROOT_DISK, confirm?" || exit 1;
echo "Here is you memory:"
free -h
read -p "What size you want for swap (eg 8G)?" SWAP_SIZE
confirm "Your swap size will be $SWAP_SIZE, confirm?" || exit 1;
echo "Here is your flake:"
nix --experimental-features 'nix-command flakes' flake show "$FLAKE_URL"
read -p "Which profile do you want to install (eg, slim)?" MACHINE
confirm "We are going to install ${FLAKE_URL}#${MACHINE}, confirm?" || exit 1;

# Unmount everything

sudo umount /mnt/boot || echo "no mounted boot partition"
sudo umount /mnt || echo "no mounted root"
sudo swapoff /dev/vg/swap || echo "no swap mounted"
sudo cryptsetup close vg-swap || echo "no swap luks to unlock"
sudo cryptsetup close vg-root || echo "no root luks to unlock"
sudo cryptsetup close root || echo "no main luks to unlock"
systemctl restart systemd-udev-trigger.service

# Format
echo "Formating..."
sudo parted -a opt --script "/dev/${ROOT_DISK}" \
mklabel gpt \
mkpart primary fat32 0% 512MiB \
mkpart primary 512MiB 100% \
set 1 esp on \
name 1 boot \
set 2 lvm on \
name 2 root

systemctl restart systemd-udev-trigger.service

sleep 5
# Cryptsetup
echo "Preparing LUKS..."
sudo cryptsetup luksFormat /dev/disk/by-partlabel/root
sudo cryptsetup luksOpen /dev/disk/by-partlabel/root root
sudo pvcreate /dev/mapper/root
sudo vgcreate vg /dev/mapper/root

sudo lvcreate -L $SWAP_SIZE -n swap vg
sync
sudo lvcreate -l '100%FREE' -n root vg
sync

# Formatting
echo "Formating LUKS..."
sudo mkfs.fat -F 32 -n boot /dev/disk/by-partlabel/boot
sudo mkfs.ext4 -L root /dev/vg/root
sudo mkswap -L swap /dev/vg/swap
sync

# Mounting
echo "Mounting..."
sudo mount /dev/disk/by-label/root /mnt
sudo mkdir -p /mnt/boot
sudo mount /dev/disk/by-label/boot /mnt/boot
sudo swapon /dev/vg/swap

old_install(){
    # Preparing to install
    echo "Installing..."
    sudo git clone $REPO_FLAKE /mnt/etc/nixos/flad

    #Install
    sudo nix-channel --add https://github.com/nix-community/home-manager/archive/release-22.11.tar.gz home-manager
    sudo nix-channel --update
    cd /mnt/etc/nixos/
    sudo ln -s flad/machines/$MACHINE/configuration.nix configuration.nix
    sudo ln -s flad/machines/$MACHINE/hardware-configuration.nix hardware-configuration.nix

    sudo nixos-install
}

new_install(){
echo "Installing..."
nix-shell -p nixFlakes --run "sudo nixos-install --flake $FLAKE_URL#$MACHINE"
}

new_install

